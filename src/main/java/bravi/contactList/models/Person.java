package bravi.contactList.models;

import javax.persistence.Column;
import javax.persistence.Entity;


@Entity
public class Person extends ModelBase {

    @Column(nullable=false, unique=true)
    String nome;

    public Person() {}

    public Person(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
