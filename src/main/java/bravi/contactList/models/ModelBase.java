package bravi.contactList.models;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;


@MappedSuperclass
@JsonIgnoreProperties(value={"dataCriacao", "dataEdicao"})
public abstract class ModelBase implements Serializable {

    @Id
    @GeneratedValue
    Long id;

    @CreatedDate
    LocalDateTime dataCriacao;

    @LastModifiedDate
    LocalDateTime dataEdicao;

    public Long getId() {
        return this.id;
    }

    public LocalDateTime getDataCriacao() {
        return this.dataCriacao;
    }

    public LocalDateTime getDataEdicao() {
        return this.dataEdicao;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDataCriacao(LocalDateTime data) {
        this.dataCriacao = data;
    }

    public void setDataEdicao(LocalDateTime data) {
        this.dataEdicao = data;
    }

}
