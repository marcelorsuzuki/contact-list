package bravi.contactList.controllers.person;

import java.util.Optional;

import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonProcessingException;

import bravi.contactList.controllers.AbstractResponse;
import bravi.contactList.models.Person;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UpdatePersonController extends AbstractResponse {

    @PutMapping("/person/{id}")
    public ResponseEntity updatePerson(
        @PathVariable Long id,
        @Valid @RequestBody Person personData
    ) throws JsonProcessingException {
        Optional<Person> person = this.personRepository.findById(id);
        if (!person.isPresent())
            this.status = HttpStatus.BAD_REQUEST;
        else {
            personData.setId(person.get().getId());
            this.personRepository.save(personData);
        }
        return new ResponseEntity<>(this.json(personData), this.status);
    }

    public String msgErro() {
        return "Person doesn't registered";
    }

}
