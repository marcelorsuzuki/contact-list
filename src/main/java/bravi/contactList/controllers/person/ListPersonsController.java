package bravi.contactList.controllers.person;

import java.util.List;


import com.fasterxml.jackson.core.JsonProcessingException;

import bravi.contactList.controllers.AbstractResponse;
import bravi.contactList.models.Person;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ListPersonsController extends AbstractResponse {

    @GetMapping("/person/all")
    public ResponseEntity listAll() throws JsonProcessingException {
        List<Person> pessoas = this.personRepository.findAll();
        return new ResponseEntity<>(this.json(pessoas), status);
    }

    public String msgErro() {
        return "";
    }

}
