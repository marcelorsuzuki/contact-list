package bravi.contactList.controllers.person;

import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;

import bravi.contactList.controllers.AbstractResponse;
import bravi.contactList.models.Person;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class GetPersonController extends AbstractResponse {

    @GetMapping("/person/{id}")
    public ResponseEntity getPerson(@PathVariable Long id) throws JsonProcessingException {
        Optional<Person> person = personRepository.findById(id);
        if (!person.isPresent())
            this.status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(this.json(person.orElse(null)), this.status);
    }

    public String msgErro() {
        return "Person doesn't registered";
    }

}
