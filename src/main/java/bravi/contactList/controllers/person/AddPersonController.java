package bravi.contactList.controllers.person;

import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonProcessingException;

import bravi.contactList.controllers.AbstractResponse;
import bravi.contactList.models.Person;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AddPersonController extends AbstractResponse {

    @PostMapping("/person")
    public ResponseEntity add(@Valid @RequestBody Person person) throws JsonProcessingException {
        Person newPerson = null;
        if (this.personRepository.findByNome(person.getNome()) != null)
            this.status = HttpStatus.BAD_REQUEST;
        else {
            newPerson = this.personRepository.save(person);
        }
        return new ResponseEntity<>(this.json(newPerson), this.status);
    }

    public String msgErro() {
        return "Person already registered";
    }

}
