package bravi.contactList.controllers.contacts;

import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;

import bravi.contactList.controllers.AbstractResponse;
import bravi.contactList.models.Contact;
import bravi.contactList.repositorys.ContactRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class DeleteContactPersonController extends AbstractResponse {

    @Autowired
    ContactRepository contactRepository;

    @DeleteMapping("/person/{id_person}/contact/{id_contato}")
    public ResponseEntity delete(
        @PathVariable Long id_person,
        @PathVariable Long id_contato
    ) throws JsonProcessingException {
        String msg = new String("Deletado com sucesso");
        Optional<Contact> contato = this.contactRepository.findByIdAndPerson_Id(
            id_contato, id_person);
        if (!contato.isPresent()) {
            this.status = HttpStatus.BAD_REQUEST;
            msg = this.msgErro();
        }
        contato.ifPresent(
            contatoConsumer -> this.contactRepository.delete(contatoConsumer));
        return new ResponseEntity<>(msg, this.status);
    }

    public String msgErro() {
        return "Person/Contato doesn't registered";
    }

}
