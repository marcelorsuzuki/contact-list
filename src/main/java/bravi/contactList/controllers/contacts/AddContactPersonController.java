package bravi.contactList.controllers.contacts;

import java.util.Optional;

import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonProcessingException;

import bravi.contactList.controllers.AbstractResponse;
import bravi.contactList.models.Contact;
import bravi.contactList.models.Person;
import bravi.contactList.repositorys.ContactRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AddContactPersonController extends AbstractResponse {

    @Autowired
    ContactRepository contactRepository;

    @PostMapping("/person/{id}/contact")
    public ResponseEntity add(
        @PathVariable Long id,
        @Valid @RequestBody Contact contact
    ) throws JsonProcessingException {
        Optional<Person> person = this.personRepository.findById(id);
        if (person.isPresent()) {
            contact.setPerson(person.get());
            this.contactRepository.save(contact);
        } else {
            this.status = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(this.json(contact), this.status);
    }

    public String msgErro() {
        return "Person doesn't registered";
    }

}
