package bravi.contactList.repositorys;

import bravi.contactList.models.Person;
import bravi.contactList.repositorys.PersonRepository;

import org.junit.runner.RunWith;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;


@DataJpaTest
@RunWith(SpringRunner.class)

public class PersonRepositoryTest {

    @Autowired
    PersonRepository personRepository;

    @Test
    public void findPersonByNameWithSuccess() {
        this.personRepository.save(new Person("Leonardo Ramos Duarte"));
        Person pessoa = this.personRepository.findByNome("Leonardo Ramos Duarte");
        Assertions.assertNotNull(pessoa);
    }

    @Test
    public void findPersonByNameWithNoSuccess() {
        this.personRepository.save(new Person("Leonardo Ramos Duarte"));
        Person pessoa = this.personRepository.findByNome("Toninho da padaria");
        Assertions.assertNull(pessoa);
    }
}
