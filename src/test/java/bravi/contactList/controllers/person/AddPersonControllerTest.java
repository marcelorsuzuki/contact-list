package bravi.contactList.controllers.person;

import bravi.contactList.controllers.BaseTestController;
import bravi.contactList.repositorys.PersonRepository;
import net.minidev.json.JSONObject;

import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;


public class AddPersonControllerTest extends BaseTestController {

    @Autowired
    PersonRepository personRepository;

    @Test
    public void cadastraNovaPessoaRecebeSucesso() throws Exception {
        HashMap<String, String> dados = new HashMap<String, String>() {{
            put("nome", "Leonardo Ramos Duarte");
        }};
        this.mockMvc.perform(
            post("/person")
            .contentType(MediaType.APPLICATION_JSON)
            .content(new JSONObject(dados).toString())
        ).andExpect(status().isOk());
    }
    
    @Test
    public void TentaCadastrarMesmaPessoaDuasVezesRecebeErro() throws Exception {
        HashMap<String, String> dados = new HashMap<String, String>() {{
            put("nome", "Leonardo Ramos Duarte");
        }};
        this.mockMvc.perform(
            post("/person")
            .contentType(MediaType.APPLICATION_JSON)
            .content(new JSONObject(dados).toString())
        ).andExpect(status().isOk());
        this.mockMvc.perform(
            post("/person")
            .contentType(MediaType.APPLICATION_JSON)
            .content(new JSONObject(dados).toString())
        ).andExpect(status().isBadRequest());
    }

}
