package bravi.contactList.controllers.person;

import bravi.contactList.controllers.BaseTestController;
import bravi.contactList.models.Person;
import bravi.contactList.repositorys.PersonRepository;
import net.minidev.json.JSONObject;

import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;


public class DeletePersonControllerTest extends BaseTestController {

    @Autowired
    PersonRepository personRepository;

    @Test
    public void deletaPessoaPorIdComSucessoRecebeOk() throws Exception {
        Person person = this.personRepository.save(new Person("Leonardo Ramos Duarte"));
        this.mockMvc.perform(
            delete(String.format("/person/%d", person.getId()))
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

    @Test
    public void tentaDeletarPessoaQueNaoExisteRecebeErro() throws Exception {
        this.mockMvc.perform(
            delete(String.format("/person/%d", 99))
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isBadRequest());
    }

}
