package bravi.contactList.controllers.contacts;

import bravi.contactList.controllers.BaseTestController;
import bravi.contactList.models.Contact;
import bravi.contactList.models.Person;
import bravi.contactList.models.Contact.TipoContato;
import bravi.contactList.repositorys.ContactRepository;
import bravi.contactList.repositorys.PersonRepository;
import net.minidev.json.JSONObject;

import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class DeleteContactPersonControllerTest extends BaseTestController {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    ContactRepository contactRepository;


    @Test
    public void tentaDeletarContatoQueNaoExisteRecebeErro() throws Exception {
        this.mockMvc.perform(
            delete("/person/%d/contact/%d", 2, 2)
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isBadRequest());
    }


    @Test
    public void DeletaContatoExisteRecebeOk() throws Exception {
    	
    	 Person person = this.personRepository.save(new Person("Leonardo Ramos Duarte"));
         Contact contato = this.contactRepository.save(
             new Contact("123123", TipoContato.TELEFONE, person));
         JSONObject json = new JSONObject();
         json.put("valor", "99999");
         json.put("tipo", 0);
         
        this.mockMvc.perform(
            delete(String.format("/person/%d/contact/%d/", person.getId(), contato.getId()))
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }
    
    
}
