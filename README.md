# Contact List

Restfull project to store persons and contacts

### Prerequisites

 - Java 1.8
 - Maven 3.5.4
 - Make 4.1
 - DockerCompose
 - psql(If you need cli with database)
 - docker 18.03.1-ce

### Installing

Installing dependencies:

```bash
:$ make instalar_dependencias
```

### Running App

This app will start after container database:

  1. Start database container.
  1. Start app

Type:
```bash
:$ make dev_server
```

### Auto open psql with credentials database

The makefile has a command to connect in postgres:

```bash
:$ make dev_server
```

**Default Password = toor**

## Running the tests

Running TestCases:

```bash
:$ make tests
```


## List endpoints

Add new person
```http
POST http://localhost:9001/person
Content-Type: application/json
User-Agent: vscode-restclient

{"nome":"Antonio Almeida Duarte"}
```


List all persons
```http
GET http://localhost:9001/person/all
Content-Type: application/json
User-Agent: vscode-restclient
```

Get one person by id
```http
GET http://localhost:9001/person/1
Content-Type: application/json
User-Agent: vscode-restclient
```

Update one person
```http
PUT http://localhost:9001/person/3
Content-Type: application/json
User-Agent: vscode-restclient

{"nome":"Antonio da Farmacia"}
```

Add new contact to a person
```http
POST http://localhost:9001/person/11/contact
Content-Type: application/json
User-Agent: vscode-restclient
{"valor":"lramosduarte@gmail.com", "tipo": 0}
```

Remove contact
```http
DELETE  http://localhost:9001/person/1/contact/5
Content-Type: application/json
User-Agent: vscode-restclient
```

Get contact
```http
GET http://localhost:9001/person/1/contact/all
Content-Type: application/json
User-Agent: vscode-restclient
```

Update contact
```http
PUT http://localhost:9001/person/1/contact/4/
Content-Type: application/json
User-Agent: vscode-restclient

{"valor":"lramosduarte@gmail.com", "tipo": 0}
```
